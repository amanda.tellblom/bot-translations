# Bot Translations

This repository hosts the translation files for all* the bots owned and managed by Lockyz Dev

Many strings are currently not able to be translated. We're working on it.

**Bots Added**
- Dismon
- JoiBoi

**Languages Currently Supported**
- en-us
- en-gb

## How to Contribute?
Currently you can create a Merge Request for new languages or strings.

## Current Contributers
- Robin "Lockyz" Painter (en-us, en-gb)

### I helped translate, where's my name?
There are multiple ways to be listed in this repository.
1 - Create the base translation for any language (Basically doing the first translation into a new language).
2 - MANY substantial contributions.

**Private Bots are only included if permission is given*
